<?PHP
//https://api.icount.co.il/api/v3.php/help/methods/doc#note-create-client-id
require 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->required(['ICOUNT_CID','ICOUNT_USER','ICOUNT_PASS']);
$dotenv->load();


$pdo = new \PDO('sqlite:paypal.db');
$fluent = new \Envms\FluentPDO\Query($pdo);


$query = $fluent->from('paypalPayments')->where('DocNum', null);

//print_r($row = $query->fetch());
// die(1);
foreach ($query as $row) {
    print_r($row);
    $json = makeInvoice($row);
    $fluent->update('paypalPayments')->set(['DocNum'=>$json['docnum']])->where('Transaction ID', $row['Transaction ID'])->execute();
}


function makeInvoice($row)
{
    $url = "https://api.icount.co.il/api/v3.php/doc/create";
    $params = array(
        // authentication
        "cid" =>  getenv('ICOUNT_CID'),
        "user" => getenv('ICOUNT_USER'),
        "pass" => getenv('ICOUNT_PASS'),

        "doctype" => "invrec",

        // client information
        "client_name" => $row['Name'], // Client name
        "email" => $row['From Email Address'], // Client email

        // additional options
        "lang" => "en",
        "currency_code" => $row['Currency'],
        "tax_exempt" => 1, // This will mark document as tax exempt

        // invoice items
        "items" => array(
            array(
                "description" => $row['Item Title'],
                "unitprice" => $row['Gross'],
                "quantity" => 1,
            ),
        ),
        'paypal' => [
            'sum'=> $row['Gross'],
            'date'=> date_create_from_format('d.m.Y', $row['Date'])->format('Ymd'),
            'txn_id'=> $row['Transaction ID'],
            'payer_name'=> $row['From Email Address'],
        ],


        // email options
        "email_to_client" => 0, // send doc to client primary email
        "email_cc_me" => 1, // send me a copy of doc email (to current user)

    );

    print_r($params);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params, null, '&'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    $info = curl_getinfo($ch);

    if (!$info["http_code"] || $info["http_code"]!=200) {
        throw new Exception("HTTP Error");
    }
    $json = json_decode($response, true);
    if (!$json["status"]) {
        print_r($json);
        throw new Exception($json["reason"]);
    }
    echo "Created invoice/receipt #".$json["docnum"];
    return $json;
}
